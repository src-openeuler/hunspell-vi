Name:           hunspell-vi
Summary:        Vietnamese hunspell dictionaries for hunspell used in OpenOffice
Version:        0.20120418
Release:        5
Source:         https://downloads.sourceforge.net/project/aoo-extensions/917/3/vi_spellchecker_ooo3.oxt
URL:            https://extensions.openoffice.org/en/project/vietnamese-spellchecker
License:        GPLv2
BuildArch:      noarch
Requires:       hunspell
Buildrequires:  git
Supplements:    (hunspell and langpacks-vi)

%description
Vietnamese hunspell dictionaries for hunspell used in OpenOffice.

%prep
%autosetup -c -n hunspell-vi -p1

%build

%install
install -d $RPM_BUILD_ROOT/%{_datadir}/myspell
install -p dictionaries/*.dic dictionaries/*.aff $RPM_BUILD_ROOT/%{_datadir}/myspell

%files
%license LICENSES-en.txt LICENSES-vi.txt
%{_datadir}/myspell/*

%changelog
* Fri Jul 30 2021 chenyanpanHW <chenyanpan@huawei.com> - 0.20120418-5
- DESC: delete -S git from %autosetup

* Tue Jun 01 2021 wulei <wulei80@huawei.com> - 0.20120418-4
- fixes failed: git no such file or directory

* Fri Apr 17 2020 Jeffery.Gao <gaojianxing@huawei.com> - 0.20120418-3
- Package init
